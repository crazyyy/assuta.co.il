<td class="LeftPanel" id="TdLeftPanel">
  <table name="TextZone1" class="grayborder" cellpadding="0" cellspacing="0" border="0" width="100%">
    <tr>
      <td>
        <table class="Feedbacktbl" cellpadding="0" cellspacing="0" border="0" align="center">
          <tr>
            <td class="FeedbackTitle">Обратная связь</td>
          </tr>
          <tr>
            <td valign="top">
              <table cellpadding="0" cellspacing="0" width="100%" border="0">
                <tr>
                  <td class="FeedbackTopText">
                    Контактная информация для
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackTopText">
                    получения бесплатной
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackTopText">
                    консультации и/или записи на
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackTopText">
                    лечение в больнице "Ассута"
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackSeparator"></td>
                </tr>
                <tr>
                  <td class="FeedbackTopText">
                    Заполните контактную форму:
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackSeparator1"></td>
                </tr>
                <tr>
                  <td>
                    <div class="FeedbackCaption"><span class="asterics">*</span> Имя, фамилия</div>
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackFormInputs">
                    <input type="Text" id="fullname" name="firstname" class="inputBoxTxt FeedbackInput">
                    <div class="FeedbackInfo" id="fullname_info" style="cursor: pointer;">
                      <img border="0" alt="" src="<?php echo get_template_directory_uri(); ?>/img/info-btn.gif" vspace="2px">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackCaption"><span class="asterics">*</span> Страна</td>
                </tr>
                <tr>
                  <td class="FeedbackFormInputs">
                    <select id="country" name="country" class="FeedbackInput">
                      <option>Выберите страну</option>
                      <option>Россия</option>
                    </select>
                    <div id="country_info" class="FeedbackInfo" style="padding-top: 5px; padding-left: 12px; cursor: pointer;">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/info-btn.gif" alt="" border="0">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackCaption"><span class="asterics">*</span> Телефон</td>
                </tr>
                <tr>
                  <td class="FeedbackFormInputs">
                    <input type="text" name="c_phone1" id="c_phone1" class="inputBoxTxt FeedbackInput" style="width:175px">
                    <div id="phone_info" class="FeedbackInfo" style="padding-top: 5px; cursor: pointer;padding-left: 6px;">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/info-btn.gif" alt="" border="0">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackCaption"><span class="asterics">*</span> E-mail</td>
                </tr>
                <tr>
                  <td class="FeedbackFormInputs" colspan="2">
                    <input type="text" name="email" id="c_email" class="inputBoxTxt FeedbackInput">
                    <div id="email_info" class="FeedbackInfo" style="padding-top: 5px; cursor: pointer;">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/info-btn.gif" alt="" border="0">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackCaption">Текст обращения</td>
                </tr>
                <tr>
                  <td class="FeedbackFormInputs">
                    <textarea name="message" id="message" cols="30" rows="3" class="inputBoxTxt1" style="width:175px"></textarea>
                    <div id="note_info" class="FeedbackInfo" style="padding-top: 5px; vertical-align: top; cursor: pointer;">
                      <img src="<?php echo get_template_directory_uri(); ?>/img/info-btn.gif" alt="" border="0">
                    </div>
                  </td>
                </tr>
                <tr>
                  <td align="left">
                    <input type="image" src="<?php echo get_template_directory_uri(); ?>/img/send_letter_ru.jpg" alt="SEND">
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackRemark" style="padding-top:10px;">поля отмеченные звёздочкой (*),
                    <br>обязательны для заполнения</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td colspan="2">
              <table cellpadding="0" cellspacing="0">
                <tr>
                  <td colspan="2" class="FeedbackPaleBlueText" valign="top" style="padding-top:10px">
                    Ул. ха-Барзель 20,
                    <br>Рамат ха-Хаяль,
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackPaleBlueText" style="padding-bottom:10px" valign="top">
                    Тель Авив, 69710 Израиль
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackGreenText" valign="top">
                    Тел.: +972-3-7643247
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackPaleBlueText" style="padding-bottom:10px" valign="top">
                    Факс: +972-3-7643248
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackPaleBlueText" valign="top">
                    Электронная почта:
                  </td>
                </tr>
                <tr>
                  <td class="FeedbackCaption" style="padding-bottom:15px" valign="top">
                    <a href="mailto:touristcenter@assuta.co.il">touristcenter@assuta.co.il</a>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</td>
